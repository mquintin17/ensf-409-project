import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * 
 */

/**
 * @author ArjunS
 *
 */
public class DBHelper implements Constants {

	private PreparedStatement us; // updateStatement
	private Connection conn;

	/**
	 * Default Constructor :Creates database and adds all the tables
	 */
	public DBHelper() {
		boolean success = false;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			try {
				conn = DriverManager.getConnection(URL + "/" + DATABASENAME, USER, PASSWORD);
			} catch (Exception e0) {
				// here => no database exists // spolution create one
				// Statement st = conn.createStatement();
				// st.executeUpdate("CREATE DATABASE "+DATABASENAME);
				us = conn.prepareStatement("CREATE DATABASE " + DATABASENAME);
				us.executeUpdate();
				success = true;
			} finally {
				System.out.println("Data Base Created");
			}
			conn = DriverManager.getConnection(URL + "/" + DATABASENAME, USER, PASSWORD);

		} catch (Exception e1) {
			success = false;
			System.out.println(e1);
			e1.printStackTrace();
		} finally {
			if (success)
				System.out.println("Connected to client database successfully");
		}
		createTables();
	}

	/**
	 * Creates all tables
	 */
	public void createTables() {

		createUserTable();
		createAssignmentTable();
		createGradeTable();
		createCourseTable();
		createSubmissinTable();
		createStudentEnrollmentTable();
	}

	/**
	 * 
	 * create a databse table of users if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createUserTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[0];

			// field1
			String field1Name = COL[0][0];
			String field1Type = "int ";
			String notNull = "NOT NULL ";

			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[0][1];
			String field2Type = "varchar(200) "; // password varcar(20) but encryopted varchar of more

			String field2 = field2Name + field2Type;

			// field3
			String field3Name = COL[0][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[0][3];
			String field4Type = "varchar(30) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[0][4];
			String field5Type = "varchar(30) ";

			String field5 = field5Name + field5Type;

			// field6
			String field6Name = COL[0][5];
			String field6Type = "char(1) ";

			String field6 = field6Name + field6Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * create a databse table of Assignment if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createAssignmentTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[1];

			// field1
			String field1Name = COL[1][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[1][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[1][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[1][3];
			;
			String field4Type = "varchar(100) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[1][4];
			;
			String field5Type = "int(1) ";

			String field5 = field5Name + field5Type + notNull;

			// field6
			String field6Name = COL[1][5];
			String field6Type = "char(16) ";

			String field6 = field6Name + field6Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;
		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * 
	 * create a databse table of Grades if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createGradeTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[2];

			// field1
			String field1Name = COL[2][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[2][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[2][2];
			String field3Type = "int(8) ";

			String field3 = field3Name + field3Type + notNull;

			// field4
			String field4Name = COL[2][3];
			String field4Type = "int(8) ";

			String field4 = field4Name + field4Type + notNull;

			// field5
			String field5Name = COL[2][4];
			String field5Type = "int(3) ";

			String field5 = field5Name + field5Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of Courses if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createCourseTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[3];

			// field1
			String field1Name = COL[3][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[3][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[3][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[3][3];
			String field4Type = "int(1) ";

			String field4 = field4Name + field4Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of Submissins if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createSubmissinTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[4];

			// field1
			String field1Name = COL[4][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[4][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[4][2];
			String field3Type = "int(8) ";

			String field3 = field3Name + field3Type + notNull;

			// field4
			String field4Name = COL[4][3];
			String field4Type = "varchar(100) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[4][4];
			String field5Type = "varchar(50) ";

			String field5 = field5Name + field5Type;

			// field6
			String field6Name = COL[4][5];
			String field6Type = "int(3) ";

			String field6 = field6Name + field6Type;

			// field7
			String field7Name = COL[4][6];
			String field7Type = "varchar(140) ";

			String field7 = field7Name + field7Type;

			// field8
			String field8Name = COL[4][7];
			String field8Type = "char(16) ";

			String field8 = field8Name + field8Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + field7 + ", " + field8 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of StudentEnrollments if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createStudentEnrollmentTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[5];

			// field1
			String field1Name = COL[5][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = "AUTO_INCREMENT ";

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[5][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[5][2];
			String field3Type = "int(8) ";
			;

			String field3 = field3Name + field3Type + notNull;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", "
					+ primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * adds a new client to database
	 * 
	 * @param us
	 *            : User to add
	 * @return
	 */
	public boolean addUser(User us) {
		String fieldData[] = { us.getPassword(), us.getEmail(), us.getFirstname(), us.getLastname(), us.getType(),
				us.getType() };
		String fieldDataString = "( '" + fieldData[0] + "' , " + "'" + fieldData[1] + "' , " + "'" + fieldData[2]
				+ "' , " + "'" + fieldData[3] + "' , " + "'" + fieldData[4] + "' , " + "'" + fieldData[5] + "' ) ";

		String fieldNameString = " ( " + COL[0][0] + " , " + COL[0][1] + " , " + COL[0][2] + " , " + COL[0][3] + " , "
				+ COL[0][4] + " , " + COL[0][5] + " ) ";

		String tableName = TABLENAMES[0];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES " + fieldDataString);
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new Assignment to database
	 * 
	 * @param as
	 *            : Assignment to add
	 * @return
	 */
//	public boolean addAssignment(Assignment as) {
//		String fieldData[] = { as.getPassword(), as.getEmail(), as.getFirstname(), as.getLastname(), as.getType(),
//				as.getType() };
//		String fieldDataString = "( '" + fieldData[0] + "' , " + "'" + fieldData[1] + "' , " + "'" + fieldData[2]
//				+ "' , " + "'" + fieldData[3] + "' , " + "'" + fieldData[4] + "' , " + "'" + fieldData[5] + "' ) ";
//
//		String fieldNameString = " ( " + COL[0][0] + " , " + COL[0][1] + " , " + COL[0][2] + " , " + COL[0][3] + " , "
//				+ COL[0][4] + " , " + COL[0][5] + " ) ";
//
//		String tableName = TABLENAMES[0];
//		boolean success = false;
//		try {
//
//			PreparedStatement ps = conn
//					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES " + fieldDataString);
//			ps.executeUpdate();
//			success = true;
//		} catch (Exception e) {
//			System.out.println(e);
//			e.printStackTrace();
//			success = false;
//		}
//		return success;
//
//	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
