interface Constants {
	final static String DRIVER = "com.mysql.jdbc.Driver";
	final static String PORT = "3306"; // for testing
	final static String URL = "jdbc:mysql://localhost:" + PORT;
	final static String USER = "root";
	final static String PASSWORD = "";
	final static String DATABASENAME = "Portal";

	final static String[] TABLENAMES = { "UserTable ", "AssignmentTable ", "GradeTable ", "CourseTable ",
			"SubmissinTable ", "StudentEnrollmentTable " }; // 6
	final static String[][] COL = { { "id ", "password ", "email ", "firstname ", "lastname ", "type " }, // 6
			{ "id ", "course_id ", "title ", "path ", "active ", "due_date " }, // 6
			{ "id ", "assign_id ", "student_id ", "course_id ", "assignment_grade " }, // 5
			{ "id ", "prof_id ", "name ", "active" }, // 4
			{ "id ", "assignment_id ", "student_id ", "path ", "title ", "submission_grade ", "comments ",
					"timeStamp " }, // 8
			{ "id ", "studen_id ", "course_id " } // 3
	};

}